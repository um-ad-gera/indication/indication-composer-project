# Composer project

Stores the foundation of the indication project.

## Local development
Follow the the steps to set up a local installation (see "Usage" section for composer
and git requirements, also make sure ddev is installed):

1. Run composer to create the project (replace siteroot with the correct directory name):
   ```
   php -d memory_limit=-1 $(which composer) create-project --stability dev uag/indication-composer-project siteroot --remove-vcs --ignore-platform-reqs --repository-url="{\"type\": \"vcs\",\"url\":  \"git@gitlab.com:um-ad-gera/indication/indication-composer-project.git\"}"
   ```
2. Go into the folder and configure a new DDEV local development environment (replace uag to your project name): 
   ```
   cd siteroot; ddev config --project-name="indication" --php-version="7.4" --mariadb-version="10.4"
   ```
3. Add your SSH keys to ddev:
   ```
   ddev auth ssh
   ```
4. Enable settings.local.php:
   ```
   mv web/sites/default/settings.local.php.example web/sites/default/settings.local.php
   ```
5. Append the following lines to `web/sites/default/settings.php`:
   ```
   if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
     include $app_root . '/' . $site_path . '/settings.local.php';
   }
   ```
6. Run `ddev start` and make sure you get a `Successfully started uag` message
7. Run `mv .ddev/commands/web/drush.example .ddev/commands/web/drush`
8. Enable stage_file_proxy (*BROKEN*):
   ```
   ddev drush sfp-en
   ```
8. Install the site (replace uag with your site name):
   * Install the site: `ddev drush si uag_simple_profile --site-name=uag`